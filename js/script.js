/*
-- Modificaciones en el DOM al hacer click en contacto --
Con este Script busco que en el momento que se da click en contacto desapaesca todo el contenido y quede un
formulario el cual pide nombre, email, telefono y consulta... esta consulta es enviada a php por medio del metodo POST
y ahi es trabajada con coneccion a una base de datos MySQL;
al costado un boton de regresar.
*/ 




//crear animacion y asignar el eventlistener al boton contactenos
var contacto = document.querySelector("#btncontacto");
contacto.addEventListener("click", showForm);

function showForm(){        
        //prueba del scroll
        scrollHash();

        //Elimina lo que hay dentro del contenedor donde va a aparecer el formulario
        var why = document.querySelector("#why");
        if(document.querySelector("#container")){
            why.removeChild(document.querySelector("#container"));
        
            //se crean los elementos necesarios para dicho formulario
            var form = document.createElement("form");
            var nombre = document.createElement("input");
            var email = document.createElement("input");
            var telefono = document.createElement("input");
            var consulta = document.createElement("textarea");
            var btn = document.createElement("button");
            var reset = document.createElement("button");

            //Se le dan atributos a las distintas etiquetas del formulario
            form.action="../php/ingresadatos.php";
            form.method="POST";
            form.classList.add("form-group");

            nombre.type="text";
            nombre.name="nombre";
            nombre.placeholder="Nombre";
            nombre.id="nombre";
            nombre.required=true;
            nombre.classList.add("form-control");

            //crear eventos para validadcion de mail 
            email.type="email";
            email.name="email";
            email.placeholder="E-mail";
            email.id="email";
            email.classList.add("form-control");
            email.classList.add("margin-twenty")
            email.required=true;

            telefono.type="text";
            telefono.name="telefono";
            telefono.placeholder="Telefeono";
            telefono.id="telefono";
            telefono.classList.add("form-control");
            telefono.classList.add("margin-twenty")
            telefono.required=false;

            consulta.type="text";
            consulta.name="consulta";
            consulta.placeholder="Escriba aqui su consulta por favor";
            consulta.id="consulta";
            consulta.classList.add("form-control");
            consulta.classList.add("margin-twenty")
            consulta.required=true;

            btn.type="submit";
            btn.innerText="Enviar";
            btn.classList.add("btn");
            btn.classList.add("btn-default");
            btn.classList.add("margin-twenty");

            reset.type="reset";
            reset.innerText="Reset";
            reset.classList.add("btn");
            reset.classList.add("btn-default");
            reset.classList.add("margin-twenty");

            form.appendChild(nombre);
            form.appendChild(email);
            form.appendChild(telefono);
            form.appendChild(consulta);
            form.appendChild(btn);
            form.appendChild(reset);

            why.appendChild(form)};
            
            
            /*** Animacion que nos lleva suavemente al contacto ***/
            function scrollHash() {
                //e.preventDefault();
                var hash = '#why';
                $('html, body').animate({
                scrollTop: $('#why').offset().top
                }, 1000, function() {
                window.location.hash = hash;
                });
         }
}