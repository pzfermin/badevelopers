/***    ABOUT US    ***/
/***    Este script va a modificar el contenedor #why ***/



var btnabout = document.querySelector("#aboutus");
btnabout.addEventListener("click", function(){

 let why = document.querySelector("#why");
/*** Preparo el entorno de trabajo***/
 why.removeChild(document.querySelector("#container"));

 /*** Creo los elementros de la seccion de Fermin ***/
 let sectionFermin      = document.createElement("section");
 let imgProfileFermin   = document.createElement("img");
 let articleFermin      = document.createElement("article")
 
 /*** Creo los elementos de la seccion de Ezequiel ***/
 let sectionEzequiel    = document.createElement("section");
 let imgProfileEzequiel = document.createElement("img");
 let articleEzequiel    = document.createElement("article");

 /*** Trabajo sobre las imgProfile ***/
 imgProfileFermin.classList.add("img-profile"); 
 imgProfileFermin.classList.add("img-fluid");
 imgProfileFermin.classList.add("img-profile-size");
 imgProfileFermin.src = "../images/fermin.jpg";
 
 imgProfileEzequiel.classList.add("img-profile");
 imgProfileEzequiel.classList.add("img-fluid");
 imgProfileEzequiel.classList.add("img-profile-size");
 imgProfileEzequiel.src = "../images/ezequiel.jpg";
 

 /*** Articulos ***/
articleFermin.classList.add("article-margin-side");
articleFermin.innerHTML="Apasionado por el desarrollo web. Actualmente cursando la carrera de <strong>Full Stack Developer</strong>. Posee solidos conocimientos en <strong>Java, JavaScript, HTML5, CSS3, MySQL, PHP y Bootstrap</strong>. Debido a la falta de oportunidades para desarrolladores sin experiencia comprobable decidio unir fuerzas con Ezequiel Lajmanovich para asi fundar <strong>HD+D<strong>"

articleEzequiel.classList.add("article-margin-side");
articleEzequiel.innerHTML="Experimentado desarrollador <strong>Drupal</strong>, con mas de 3 años de experiencia y diversa cantidad de proyectos para sitios institucionales y de comercio electronico. Algunas de sus herramientas principales son <strong>PHP, MYSQL, HTML5, CSS3, Jquery y JavaScript</strong>. Ezequiel cuenta con una increible capacidad de hacer frente a la presión y ofrecer resultados de alta calidad.";


 /*** Anidamiento de etiquetas ***/
 sectionFermin.classList.add("flex-content-start");
 sectionFermin.appendChild(imgProfileFermin);
 sectionFermin.appendChild(articleFermin);

 

 sectionEzequiel.classList.add("flex-content-start")
 sectionEzequiel.appendChild(imgProfileEzequiel);
 sectionEzequiel.appendChild(articleEzequiel);
 
 
 
 why.appendChild(sectionFermin);
 why.appendChild(sectionEzequiel);
 
}
)