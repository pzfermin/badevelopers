<?php
    /*************************DESCRIPCION DEL CODIGO ****************************
     *  Se crea una clase para trabajar con dos metodos esenciales. 
     *  Uno nos permite tomar los datos de una DB
     *  El otro nos permite ingresarlos
     *  Hay metodos que se encargan de analizar los datos ingresados por el usuario
     *  para evitar ataques de tipo SQL Injection
     *  Hacemos recomendaciones para mejorar el codigo y la seguridad del mismo 
     *  Gracias por leer mi codigo, que tengas un buen dia!
     *  Mail: pzfermin@gmail.com 
    **************************DESCRIPCION DEL CODIGO ****************************/
    class Contacto
    {
        private function _construct(){}
        private $nombre;
        private $telefono;
        private $email;
        private $consulta;
        
        /* METODO PARA LISTAR LOS DATOS DE LA BASE DE DATOS */
        public function listarContactos()
        {
            $link = Coneccion::conectar();
            $sql  = "SELECT Nombre, Email, Telefono, Consulta FROM contacto";
            $stmt = $link->prepare($sql);
            $stmt->execute();

            return $listado = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        /* METODOS SETTERS Y GETTERS */
        public function getNombre()
        {
                return $this->nombre;
        }

  
        public function setNombre($nombre)
        {
               if(strlen($nombre)<50 )
                      $this->nombre = filter_var($nombre, FILTER_SANITIZE_STRING);
                     
        }

        public function getTelefono()
        {
                return $this->telefono;          
        }

        public function setTelefono($telefono)
        {
                if(is_numeric($telefono) && strlen($telefono)<20)
                        $this->telefono = $telefono;
                      
        }

        public function getEmail()
        {
                return $this->email;
        }

        public function setEmail($email)
        {
                if($email!=null && strlen($email)<50)
                        $this->email = filter_var($email,FILTER_VALIDATE_EMAIL);  
                                    
        }

        public function getConsulta()
        {
                return $this->consulta;
        }

        public function setConsulta($consulta)
        {
                if($consulta!=null && strlen($consulta)<200 && $this->caracateresPermitidos($consulta)!==false)
                    $this->consulta = $consulta;
                    
        }


        /* METODO PARA LA INSERSION DE DATOS POR MEDIO DE SQL*/
        public function cargarDatos($nombre, $email, $telefono, $consulta)
        {
                $link = Coneccion::conectar();
           
                if(($nombre && $email && $telefono && $consulta)!=null)
                {
                        $this->setNombre($nombre);
                        $this->setEmail($email);
                        $this->setTelefono($telefono);
                        $this->setConsulta($consulta);
                        if(isset($this->nombre)&&isset($this->email)&&isset($this->telefono)&&isset($this->consulta))
                        {
                       
                                $sql = "INSERT INTO Contacto VALUES (:nombre,:email,:telefono,:consulta)";
                                $stmt = $link->prepare($sql);

                                $cNombre = $this->getNombre();
                                $stmt->bindParam(':nombre', $cNombre, PDO::PARAM_STR);
                                $cEmail = $this->getEmail();
                                $stmt->bindParam(':email', $cEmail, PDO::PARAM_STR);
                                $cTelefono = $this->getTelefono();
                                $stmt->bindParam(':telefono', $cTelefono, PDO::PARAM_INT);
                                $cConsulta = $this->getConsulta();
                                $stmt->bindParam(':consulta', $cConsulta, PDO::PARAM_STR);
                                $stmt->execute();
                                //echo "la inserción se ha realizado correctamente";
                                                 
                        }
                        else
                        {
                                echo "la insersion no se ha realizado";
                        }
                }
               
        }
        

        /* FUNCION PARA ANALIZAR LOS DATOS DE ENTRADA */
        public function caracateresPermitidos($nombre)
        {
                $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ ";
                for ($i=0; $i<strlen($nombre); $i++)
                { 
                        if (strpos($permitidos, substr($nombre,$i,1))===false)
                        { 
                                return false; 
                                echo "la condicion ".$nombre."es falsa";
                        } 
                } 
        }

        public function caracateresComentarios($nombre)
        {
                $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ ?";
                for ($i=0; $i<strlen($nombre); $i++)
                { 
                        if (strpos($permitidos, substr($nombre,$i,1))==false)
                        { 
                                return false; 
                                echo "la condicion ".$nombre."es falsa";
                        } 
                } 
        }
    }
    ?>